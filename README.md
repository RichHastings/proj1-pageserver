# README # 
 
## Author: Rich Hastings, ghastings@uoregon.edu ##

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

 
Basic web server will return file contents if file exists. 
Otherwise it will return 404 error.
if requested file contains "..", "//"", or "~"  will return 403 error.